class Nested::BaseController < ApplicationController
  layout 'application'

  before_action :set_nested_book, only: %i[ show edit update destroy ]

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nested_book
      @nested_book = Nested::Book.find(params[:id])
    end
end
