class Nested::BooksController < Nested::BaseController
  # GET /nested/books or /nested/books.json
  def index
    @nested_books = Nested::Book.all
  end

  # GET /nested/books/1 or /nested/books/1.json
  def show
    render @nested_book
  end

  # GET /nested/books/new
  def new
    @nested_book = Nested::Book.new
  end

  # GET /nested/books/1/edit
  def edit
  end

  # POST /nested/books or /nested/books.json
  def create
    @nested_book = Nested::Book.new(nested_book_params)

    respond_to do |format|
      if @nested_book.save
        format.html { redirect_to nested_book_url(@nested_book), notice: "Book was successfully created." }
        format.json { render :show, status: :created, location: @nested_book }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @nested_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /nested/books/1 or /nested/books/1.json
  def update
    respond_to do |format|
      if @nested_book.update(nested_book_params)
        format.html { redirect_to nested_book_url(@nested_book), notice: "Book was successfully updated." }
        format.json { render :show, status: :ok, location: @nested_book }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @nested_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /nested/books/1 or /nested/books/1.json
  def destroy
    @nested_book.destroy

    respond_to do |format|
      format.html { redirect_to nested_books_url, notice: "Book was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Only allow a list of trusted parameters through.
    def nested_book_params
      params.fetch(:nested_book, {})
    end
end
