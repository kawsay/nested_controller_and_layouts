json.extract! nested_book, :id, :created_at, :updated_at
json.url nested_book_url(nested_book, format: :json)
