require "test_helper"

class Nested::BooksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @nested_book = nested_books(:one)
  end

  test "should get index" do
    get nested_books_url
    assert_response :success
  end

  test "should get new" do
    get new_nested_book_url
    assert_response :success
  end

  test "should create nested_book" do
    assert_difference("Nested::Book.count") do
      post nested_books_url, params: { nested_book: {  } }
    end

    assert_redirected_to nested_book_url(Nested::Book.last)
  end

  test "should show nested_book" do
    get nested_book_url(@nested_book)
    assert_response :success
  end

  test "should get edit" do
    get edit_nested_book_url(@nested_book)
    assert_response :success
  end

  test "should update nested_book" do
    patch nested_book_url(@nested_book), params: { nested_book: {  } }
    assert_redirected_to nested_book_url(@nested_book)
  end

  test "should destroy nested_book" do
    assert_difference("Nested::Book.count", -1) do
      delete nested_book_url(@nested_book)
    end

    assert_redirected_to nested_books_url
  end
end
