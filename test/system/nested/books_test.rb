require "application_system_test_case"

class Nested::BooksTest < ApplicationSystemTestCase
  setup do
    @nested_book = nested_books(:one)
  end

  test "visiting the index" do
    visit nested_books_url
    assert_selector "h1", text: "Books"
  end

  test "should create book" do
    visit nested_books_url
    click_on "New book"

    click_on "Create Book"

    assert_text "Book was successfully created"
    click_on "Back"
  end

  test "should update Book" do
    visit nested_book_url(@nested_book)
    click_on "Edit this book", match: :first

    click_on "Update Book"

    assert_text "Book was successfully updated"
    click_on "Back"
  end

  test "should destroy Book" do
    visit nested_book_url(@nested_book)
    click_on "Destroy this book", match: :first

    assert_text "Book was successfully destroyed"
  end
end
