Rails.application.routes.draw do
  root to: 'nested/books#index'

  namespace :nested do
    resources :books
  end
end
